/**
* Main class for the game
* @author Nico Funke
*/

package geosweep;

import geosweep.board.*;
import geosweep.gui.*;

public class Main{

	/** Just starts the jframe **/
	public static void main(String args[]){
		new MainFrame();
	}
}