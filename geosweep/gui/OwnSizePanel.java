package geosweep.gui;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

import static javax.swing.JOptionPane.*;

/** Panel to choose the boardsize for own scenario **/
public class OwnSizePanel extends javax.swing.JPanel {

	JTextField breiteField = new JTextField("9");
	JTextField laengeField = new JTextField("9");
	JTextField minenField = new JTextField("10");

    /**
     * Empty constructor constructs the panel
     */
    public OwnSizePanel() {
        this.setOpaque(false);
        this.setLayout(new GridLayout(3,4));
        

		// --- Save button
        JButton  saveButton = LauncherPanel.createFancyButton("Weiter","#f796b");
        saveButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize();
		  }
		});

		JButton  backButton = LauncherPanel.createFancyButton("Zurück","#777777");
        backButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    backButton();
		  }
		});

        // --- Textfields
		Font font1 = new Font("Times New Roman", Font.PLAIN, 18);

		breiteField.setFont(font1);
		laengeField.setFont(font1);
		minenField.setFont(font1);
		breiteField.setBackground(Color.BLACK);
		laengeField.setBackground(Color.BLACK);
		minenField.setBackground(Color.BLACK);
		breiteField.setForeground(Color.WHITE);
		laengeField.setForeground(Color.WHITE);
		minenField.setForeground(Color.WHITE);
		breiteField.setHorizontalAlignment(JTextField.CENTER);
		laengeField.setHorizontalAlignment(JTextField.CENTER);
		minenField.setHorizontalAlignment(JTextField.CENTER);


		this.add(LauncherPanel.createFancyButton("Breite:","#c1ad2a"));
		this.add(breiteField);
		this.add(Box.createVerticalStrut(20));
		this.add(saveButton);
		
		this.add(Box.createVerticalStrut(20));
		this.add(LauncherPanel.createFancyButton("Länge:","#ef796b"));
		this.add(laengeField);
		this.add(Box.createVerticalStrut(20));	
		this.add(backButton);
		this.add(Box.createVerticalStrut(20));
		this.add(LauncherPanel.createFancyButton("Minen:","#c1ad2a"));
		this.add(minenField);

    }

    /**
     * Sends the chosen size to the launcherpanel
     * After the the Launcherpanel will change and this panel will disappear
     */
    public void saveSize(){
    	int x;
    	int y; 
    	int mines;
    	try{
    		x = Integer.parseInt(breiteField.getText());
    		y = Integer.parseInt(laengeField.getText()); 
    		mines = Integer.parseInt(minenField.getText());	
    	} catch(NumberFormatException e){
    		showMessageDialog(null, "Bitte gib eine gültige Zahl ein!", "Fehler", JOptionPane.ERROR_MESSAGE);
    		return;
    	}
    	if( x * y <= mines){
    		showMessageDialog(null, "Bitte gib mehr Felder als Minen an!", "Fehler", JOptionPane.ERROR_MESSAGE);
    		return;	
    	}
    	if( x<=0 || y <= 0 || mines < 0){
    		showMessageDialog(null, "Bitte nur positive Zahlen eingeben!", "Fehler", JOptionPane.ERROR_MESSAGE);
    		return;
    	}
    	((LauncherPanel)this.getParent()).setSize(x,y, mines);
    }

    /**
     * Notifies the Launcherpanel to reopen the sizepanel
     */
    public void backButton(){
    	((LauncherPanel)this.getParent()).showSizePanel();
    }

}

