package geosweep.gui;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;

public class FootPanel extends javax.swing.JPanel {
    
    public FootPanel() {
        super(true);
        this.setLayout(new BorderLayout());
		this.add(new JLabel("<html><span style='font-size:9px; color: #dadada;'>"+"Nico Funke 2018"+"</span></html>"), BorderLayout.CENTER);
        this.setBackground(Color.BLACK);
    }

}