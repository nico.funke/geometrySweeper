package geosweep.gui;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

/** Panel to choose the boardsize or own scenario **/
public class SizePanel extends javax.swing.JPanel {
    
    /**
     * Empty constructor constructs the panel
     */
    public SizePanel() {
        this.setOpaque(false);
        this.setLayout(new GridLayout(5,2));
        
        // --- EASY BUTTONS
        JButton  easyRect = LauncherPanel.createFancyButton("81 Felder, 10 Minen","#f796b");
        easyRect.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(9,9,10);
		  }
		});
        JButton  easyRect2 = LauncherPanel.createFancyButton("81 Felder, 35 Minen","#f796b");
        easyRect2.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(9,9,35);
		  }
		});

        // --- medium buttons
        JButton  mediumRect = LauncherPanel.createFancyButton("256 Felder, 40 Minen","#c1ad2a");
        mediumRect.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(16,16,40);
		  }
		});
        JButton  mediumRect2 = LauncherPanel.createFancyButton("256 Felder, 99 Minen","#c1ad2a");
		mediumRect2.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(16,16,99);
		  }
		});

		// --- profi buttons
        JButton  profiRect = LauncherPanel.createFancyButton("480 Felder, 99 Minen","#ef796b");
        profiRect.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(24,20,99);
		  }
		});
        JButton  profiRect2 = LauncherPanel.createFancyButton("480 Felder, 170 Minen","#ef796b");
        profiRect2.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    saveSize(24,20,170);
		  }
		});

		// --- OWN SCENARIO BUTTON
        JButton  ownButton = LauncherPanel.createFancyButton("Eigenes Szenario","#39a6a8");
        ownButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		    ownScenario();
		  }
		});
		this.add(easyRect);
		this.add(Box.createVerticalStrut(20));
		this.add(easyRect2);
		this.add(Box.createVerticalStrut(20));
		this.add(mediumRect);
		this.add(Box.createVerticalStrut(20));	
		this.add(mediumRect2);
		this.add(Box.createVerticalStrut(20));
		this.add(profiRect);
		this.add(Box.createVerticalStrut(20));
		this.add(profiRect2);
		this.add(Box.createVerticalStrut(20));
		this.add(Box.createVerticalStrut(20));
		this.add(Box.createVerticalStrut(20));
		this.add(ownButton);
    }	

    /**
     * Sends the chosen size to the launcherpanel
     * After the the Launcherpanel will change and this panel will disappear
     * @param x			length of board
     * @param y 		height of board
     * @param mines 	number of mines
     */
    public void saveSize(int x, int y, int mines){
    	((LauncherPanel)this.getParent()).setSize(x,y, mines);
    }

    /** 
   	 * Method if the user chose an own scenario
   	 */
    public void ownScenario(){
    	((LauncherPanel)this.getParent()).startOwnSizeSelection();	
    }
}