package geosweep.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BoardMouseAdapter extends MouseAdapter{
	
	/**
	 * Method to handle mouseclicks
	 * @param e 	MouseEvent
	 */
	public void mouseClicked(MouseEvent e) {
        int x=e.getX();
        int y=e.getY();
        
        BoardPanel component = (BoardPanel) e.getComponent();
        if (e.getModifiers() == MouseEvent.BUTTON3_MASK){
      		component.rightClick(x,y);
    	} else if(e.getClickCount() == 2) {
            component.doubleClick(x,y);
        }else {
    		component.leftClick(x,y);
    	}
    }	
}