package geosweep.gui;

import geosweep.board.Board;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;

public class MainFrame extends javax.swing.JFrame {
    

    /**
     * Empty constructor.
     * Starts the launcher
     */
    public MainFrame () {
        super("GeoSweep");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(850, 600);
        setLocationRelativeTo(null);

        //Image icon = new javax.swing.ImageIcon("android.png").getImage();
		//this.setIconImage(icon);

        getContentPane().setBackground(Color.BLACK);


		this.setLayout(new BorderLayout());
        this.add(new LauncherPanel(), BorderLayout.CENTER);
        this.add(new JLabel("<html><span style='text-align: right;font-size:9px; color: #f0f0f0;width:100%;'>"+"Nico Funke 2018"+"</span></html>",SwingConstants.RIGHT), BorderLayout.PAGE_END);
        this.add(new JLabel("<html><span style='font-size:30px; color: #f0f0f0;'>"+"GeoSweep"+"</span></html>"), BorderLayout.PAGE_START);

		this.setVisible(true);
    }

    /**
     * Constructor with a board. Starts a game
     * @param board  board to be shown
     */
    public MainFrame (Board b) {
        super("GeoSweep");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(850, 600);
        setLocationRelativeTo(null);

        //Image icon = new javax.swing.ImageIcon("android.png").getImage();
        //this.setIconImage(icon);

        getContentPane().setBackground(Color.BLACK);


        this.setLayout(new BorderLayout());
        BoardPanel p = new BoardPanel(b);
        this.add(p, BorderLayout.CENTER);
        // this.add(new LauncherPanel(), BorderLayout.CENTER);
        this.add(new JLabel("<html><span style='text-align: right;font-size:9px; color: #f0f0f0;width:100%;'>"+"Nico Funke 2018"+"</span></html>",SwingConstants.RIGHT), BorderLayout.PAGE_END);
        this.add(new JLabel("<html><span style='font-size:30px; color: #f0f0f0;'>"+"GeoSweep"+"</span></html>"), BorderLayout.PAGE_START);

        this.setVisible(true);
    }

    /**
     * Starts a new MainFrame with the values given
     * @param x     width of the board
     * @param y     height of the board
     * @param mines mines to be placed
     * @param shape shape of the board
     */
    public void startGame(int x, int y, int mines,int shape){
        Board b = new Board(x,y,shape,mines);
        new MainFrame(b);      
    }


}
