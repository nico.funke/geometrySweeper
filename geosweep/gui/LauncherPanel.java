package geosweep.gui;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import javax.swing.border.EmptyBorder;

public class LauncherPanel extends javax.swing.JPanel {
    
    private static int height	=-1;
    private static int width	=-1;
    private static int mines	=-1;
    private static int shape 	=-1;

    /**
     * Empty constructor, starts a panel for size chosing
     */
    public LauncherPanel() {
        super(true);
        this.setOpaque(false);
        showSizePanel();   
    }

    /**
     * Saves the sizes of the board and starts a panel to chose the shape
     * @param x     width of board
     * @param y     height of board
     * @param shape shape of board
     */
    public void setSize(int x,int y, int mines){
        this.mines = mines;
        this.width = x;
        this.height= y;
        removeAll();
        this.setLayout(new GridLayout(1,1));
        this.add(new ShapePanel());
        this.revalidate();
        this.repaint();
    }

    /**
     * Starts a Panel to select own board sizes
     */
    public void startOwnSizeSelection(){
        removeAll();
        this.setLayout(new GridLayout(1,1));
        this.add(new OwnSizePanel());
        this.revalidate();
        this.repaint();
    }

    /**
     * Shows the size panel
     */
    public void showSizePanel(){
        removeAll();
        this.setLayout(new GridLayout(1,1));
        this.add(new SizePanel());
        this.revalidate();
        this.repaint();
    }

    /**
     * saves the shape and notifies the mainframe
     * @param shape     shape of the board
     */
    public void setShape(int shape ){
        this.shape = shape;
        ((MainFrame)SwingUtilities.getWindowAncestor(this)).startGame(width,height, mines,shape);
    }

    /**
     * Creates a fancy JButton
     * @param text 	text to be displayed on the button
     * @return 	fancy jbutton
     */
    public static JButton createFancyButton(String text, String color){
    	JButton button = new JButton(text);
        button.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        button.setBackground(Color.decode(color));
        button.setForeground(Color.white);
        button.setUI(new myButtonUI());
        // button.setAlignmentX(Component.CENTER_ALIGNMENT);
        return button;
    }



}
