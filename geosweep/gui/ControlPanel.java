package geosweep.gui;
import geosweep.board.Board;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

/** Panel to choose the shape **/
public class ControlPanel extends javax.swing.JPanel {
    
    /**
     * Empty constructor constructs the panel
     */
    public ControlPanel() {
        this.setOpaque(false);
        this.setLayout(new GridLayout(1,6));
        JButton  back = LauncherPanel.createFancyButton("Rückgängig","#f796b");
        back.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
            undo();
          }
        });

        JButton  rest = LauncherPanel.createFancyButton("Neustart","#ef796b");
        rest.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
            restart();
          }
        });

		this.add(back);
		this.add(Box.createVerticalStrut(12));	
        this.add(Box.createVerticalStrut(12));
        this.add(Box.createVerticalStrut(12));
		this.add(Box.createVerticalStrut(12));	
		this.add(rest);
    }

    public void undo(){
        ((BoardPanel)this.getParent()).undo();
    }

    public void restart(){
        ((BoardPanel)this.getParent()).restart();
    }

}