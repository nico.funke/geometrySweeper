package geosweep.gui;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;

public class TitlePanel extends javax.swing.JPanel {
    
    public TitlePanel() {
        super(true);
        this.setLayout(new BorderLayout());
		this.add(new JLabel("<html><span style='font-size:20px; color: #dadada;'>"+"GeoSweep"+"</span></html>"), BorderLayout.CENTER);
        this.setBackground(Color.BLACK);
    }

}