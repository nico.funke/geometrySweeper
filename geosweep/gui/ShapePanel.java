package geosweep.gui;
import geosweep.board.Board;

import javax.swing.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

/** Panel to choose the shape **/
public class ShapePanel extends javax.swing.JPanel {
    
    /**
     * Empty constructor constructs the panel
     */
    public ShapePanel() {
        this.setOpaque(false);
        this.setLayout(new GridLayout(3,3));
        JButton  hex = LauncherPanel.createFancyButton("Hexagon Spiel","#f796b");
        hex.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
            saveShape(Board.SHAPE_HEX);
          }
        });

        JButton  backButton = LauncherPanel.createFancyButton("Zurück","#777777");
        backButton.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
            backButton();
          }
        });


        JButton  rect = LauncherPanel.createFancyButton("Rechteckiges Spiel","#ef796b");
        rect.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
            saveShape(Board.SHAPE_RECT);
          }
        });

		this.add(hex);
		this.add(Box.createVerticalStrut(20));
        this.add(Box.createVerticalStrut(20));

        
		this.add(Box.createVerticalStrut(20));	
		this.add(rect);
        this.add(Box.createVerticalStrut(20));
        this.add(Box.createVerticalStrut(20));
        this.add(Box.createVerticalStrut(20));
        this.add(backButton);
    }

    /**
     * Sends the chosen shape to the launcherpanel
     * After the the Launcherpanel will change and this panel will disappear
     * @param shape         shape of board
     */
    public void saveShape(int shape){
        ((LauncherPanel)this.getParent()).setShape(shape);
    }

    /**
     * Notifies the Launcherpanel to reopen the sizepanel
     */
    public void backButton(){
        ((LauncherPanel)this.getParent()).showSizePanel();
    }
}