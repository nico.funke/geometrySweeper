package geosweep.gui;

import geosweep.board.Board;
import geosweep.board.Field;

import java.time.Duration;
import java.util.Random;
import java.util.Calendar;
import java.util.Date;
import javax.swing.*;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;

import static javax.swing.JOptionPane.*;
import java.awt.image.BufferedImage;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.FontMetrics;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.PointerInfo;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BoardPanel extends javax.swing.JPanel {

	private Board board;
    private int width;
    private Timer timer;
    private JLabel infoLabel;
    private Date start;
    private boolean end = false;

    /**
     * Empty Constructor, that adds the titles and background
     */
    public BoardPanel() {
        super(true);
        this.setBackground(Color.BLACK);
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
       	
		
    }

    /**
     * Constructor with a board to play on
     * @param b 	board to play on  
     */
    public BoardPanel(Board b){
    	super(true);
        this.setBackground(Color.BLACK);
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
       	this.add(new ControlPanel(), BorderLayout.PAGE_START);
    	infoLabel = new JLabel();
        this.add(infoLabel, BorderLayout.PAGE_END);
        addTimer();
        board = b;

        addMouseListener(new BoardMouseAdapter()); 
    }

    /**
     * Adds a timer to the panel,
     * that updates the infolabel
     */
    private void addTimer(){
        start = new Date();
        ActionListener updateClockAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long diffInMillies = Math.abs(new Date().getTime() - start.getTime());
                infoLabel.setText("<html><span style='font-size:12px; color: #b0b0b0;'>Time: "
                    +diffInMillies/1000+" Clicks: "+board.getClicks()
                    +"</span></html>"); 
            }
        };
        timer = new Timer(250, updateClockAction);
        timer.start();
    }

    /**
     * Undoes the last step
     */
    public void undo(){
        if(end){
            end = false;
            timer.start();
        }
        board.undo();
        repaint();

    }

    /*
     * restarts the games
     */
    public void restart(){
        board.reset();
        addTimer();
        end = false;
        repaint();
    }

    /**
     * Draws the background for the panel
     * @param g 	graphics object
     */
    private void drawBackground(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.GRAY);
        for (int i = 0; i <= 1000; i++) {
            int w = getFrameWidth();
            int h = getFrameHeight();

            Random r = new Random();
            int x = Math.abs(r.nextInt()) % w;
            int y = Math.abs(r.nextInt()) % h;
            g2d.drawLine(x, y, x+2, y+2);
            g2d.drawLine(x+2, y, x, y+2);
        }
    }

    /**
     * Returns the width of the frame
     * @return  width
     */
    private int getFrameWidth(){
      	Dimension size = getSize();
      	Insets insets = getInsets();

      	return size.width - insets.left - insets.right;
    }

    /**
     * Returns the height of the frame
     * @return  height
     */
    private int getFrameHeight(){
      	Dimension size = getSize();
      	Insets insets = getInsets();
      	return size.height - insets.top - insets.bottom;
    }



    /**
     * Draws a hexagon
     * @param x     x position of the center
     * @param y     y position of the center
     * @param width width of the hexagon
     * @param g     graphics object
     */
    private void drawHexagon(int x,int y, int width, Graphics g, Color c, String t){
    	if(t.equals("0")){
      		return;
      	}
      	int a = (width/2) -1;

        // Shadow
        g.setColor(c.darker());
        int[] xPoints2 = {x-a/2+1,x+a/2+1,x+a+1,x+a/2+1,x-a/2+1,x-a+1 };
        int[] yPoints2 = {y-a+1,y-a+1,y+1,y+a+1,y+a+1,y+1};
        g.fillPolygon(xPoints2,yPoints2,6);

        // Main Hex
      	g.setColor(c);
      	int[] xPoints = {x-a/2,x+a/2,x+a,x+a/2,x-a/2,x-a };
      	int[] yPoints = {y-a,y-a,y,y+a,y+a,y};
      	g.fillPolygon(xPoints,yPoints,6);


       	g.setColor(Color.BLACK);

        int size = (int) (width/2.5);
        if(t.equals("☠") || t.equals("☢")){
            size += 8;
        }
  		g.setFont(new Font("TimesRoman", Font.PLAIN, size));
       	g.drawString(t,x,y);
    }

    /**
     * Draws a hex-shaped board
     * @param g     graphics object
     */
    private void drawHexBoard(Graphics g){
    	int x = this.board.getWidth();
    	int y = this.board.getHeight();
    	int[][] view = this.board.getView();
        width =(int)( (getFrameWidth()) / (x*1.5+1.75));

        if((getFrameHeight()-80)/(1 + y/2) < width){
          width =(int) ( (getFrameHeight()-80)/(1 + y/2));
        }
        for(int j = 0; j < y; j++){
            for(int i = 0; i < x; i++){

                Color c = getColor(view[i][j]);
                Integer[] center = getCoordinates(i,j);
                String t = getContent(view[i][j]);
                
                drawHexagon(center[0] ,center[1], width,g,c, t);
            }
        }
    }


    /**
     * Performs a right click on the coordinates
     * @param x        x coordinates of the click
     * @param y        y coordinates of the click
     */
    public void rightClick(int x, int y){
        Integer[] position = getField(x,y);
        if(position == null){
            return;
        }
        board.mark(position[0],position[1]);
        evaluate();
        repaint();
    }

    /**
     * Performs a double click on the coordinates
     * @param x        x coordinates of the click
     * @param y        y coordinates of the click
     */
    public void doubleClick(int x, int y){
        Integer[] position = getField(x,y);
        if(position == null){
            return;
        }
        board.doubleClick(position[0],position[1]);
        evaluate();
        repaint();
    }

    /**
     * Performs a left click on the coordinates
     * @param x        x coordinates of the click
     * @param y        y coordinates of the click
     */
    public void leftClick(int x, int y){
        Integer[] position = getField(x,y);
        if(position  == null){
            return;
        }
        board.click(position[0],position[1]);
        repaint();
        evaluate();
    }

    /** 
     * Evaluates if the game is over, stops the timer and shows an dialog
     */
    public void evaluate(){
        if( board.getStatus() == Board.STATUS_GAME || end){
            return;
        }
        timer.stop();
        if(board.getStatus() == Board.STATUS_LOSE){
            showMessageDialog(null, "Wie Schade! Du hast leider verloren. Hättest du mal lieber nicht auf die Mine geklickt :(", "Ende der Runde!", JOptionPane.PLAIN_MESSAGE);
        } else {
            long seconds = Math.abs(new Date().getTime() - start.getTime())/1000;
            showMessageDialog(null, "Herzlichen Glückwunsch! Du hast gewonnen! \nUnd das mit "+board.getClicks()
                +" Clicks in einer Zeit von "+seconds+" Sekunden.",
                "Ende der Runde!", JOptionPane.PLAIN_MESSAGE);
        }
        end = true;
    }

    /**
     * Calculates the position of the field from coordinates
     * calculation is done via finding the coordinates with the minimal euclidean distance
     * @param x     x coordinates
     * @param y     y coordinates
     * @return      position on the board
     **/
    private Integer[] getField( int x, int y){
        if(this.board == null){
            throw new NullPointerException("No Board was initialised!");
        } 
        double minDist = Double.MAX_VALUE;
        Integer[] min = {0, 0};

        for(int i=0; i < board.getHeight() ; i ++){
            for(int j=0; j < board.getWidth() ; j ++){
                Integer[] center = getCoordinates(j,i);
                double dist = getDistance( center[0], center[1], x, y); 
                if( dist < minDist){
                    min[0] = j;
                    min[1] = i;
                    minDist = dist;
                }
            }
        }
        if(minDist < 1 +  width/2){
            return min;    
        }
        return null;
    }

    /**
     * Returns the coordinates of a specific field on the board
     * @param x     x position of the field
     * @param y     y position of the field
     * @return      coordinates of the centrum of the field
     */
    private Integer[] getCoordinates(int x, int y){
        if(this.board == null){
            throw new NullPointerException("No Board was initialised!");
        } else if(this.board.getGeometry() == Board.SHAPE_RECT){
            Integer[] out = { 20 + width*x + 1+(int)(0.5*width) , 
                              60 + width*y + 1+(int)(0.5*width) };
            return out;
        } else if(this.board.getGeometry() == Board.SHAPE_HEX){
            Integer[] out = { (int)(1.5*width*x + ((y+1)%2) * 0.75*width + 0.5*width)+20 ,
                              (int) ((1+width/2)*y +(int)(0.5*width)+  40)};
            return out;
        } else if(this.board.getGeometry() == Board.SHAPE_TRIANGLE){
            // TODO Triangle 
        }
        return null;
    }

    /**
     * Returns the color for the fieldstatus
     * @param fieldstatus  fieldstatus
     * @return  fitting color
     */
    private Color getColor(int fieldStatus){
        Color c = Color.WHITE;
        switch(fieldStatus){
            case Field.EXPLODED:
                c = Color.decode("#a31d00");
                break;
            case Field.QUESTION:
                c = Color.decode("#f5ff9b");
                break;
            case Field.MARKED:
                c = Color.decode("#ea5600");
                break;
        }
        if(fieldStatus > 0 ){
            c = Color.decode("#b3ffb3");
            if(fieldStatus > 1){
                c = Color.decode("#66ff66");
                if(fieldStatus > 2){
                    c = Color.decode("#1aff1a");
                }
            }
        }
        return c;
    }

    /**
     * Returns the contentString for the fieldstatus
     * @param fieldstatus  fieldstatus
     * @return  fitting string
     */
    public String getContent( int fieldStatus){
        if(fieldStatus >= 0){
            return "" + fieldStatus;
        }
        if(fieldStatus == Field.EXPLODED){
            return "";
        }
        if(fieldStatus == Field.MARKED){
            return "";
        }
        if(fieldStatus == Field.QUESTION){
            return "?";
        }
        return "";
    }

    /**
     * Draws a rectangular-shaped board
     * @param g     graphics object
     * @param x     number of columns
     * @param y     number of rows
     */
    private void drawRectBoard(Graphics g){
    	int x = this.board.getWidth();
    	int y = this.board.getHeight();
        width =  (getFrameWidth()-70) / x;
        int[][] view = this.board.getView();

        if((getFrameHeight()-90) / y < width ){
        	width = (getFrameHeight()-90) / y;
        }
        for(int j = 0; j < y; j++){
            for(int i = 0; i < x; i++){
                
                Color c = getColor(view[i][j]);
                Integer[] center = getCoordinates(i,j);
                String t = getContent(view[i][j]);

                drawRect( center[0],center[1],width,g, c,t);
            }
        }
    }

    /**
     * Draws a square
     * @param x     number of columns
     * @param y     number of rows
     * @param width width of the square
     * @param g     graphics object
     */
    private void drawRect(int x,int y, int width, Graphics g, Color c, String t){
        if(t.equals("0")){
            return;
        }
        int a = width/2-1;

        g.setColor(c.darker());
        int[] xPoints3 = {x-a+1,x+a+1,x+a+1,x-a+1};
        int[] yPoints3 = {y-a+1,y-a+1,y+a+1,y+a+1};
        g.fillPolygon(xPoints3,yPoints3,4);

        g.setColor(c);
        int[] xPoints2 = {x-a,x+a,x+a,x-a};
        int[] yPoints2 = {y-a,y-a,y+a,y+a};
        g.fillPolygon(xPoints2,yPoints2,4);

        g.setColor(Color.BLACK);
        int size = (int) (width/2.5);
        if(t.equals("☠") || t.equals("☢")){
            size += 8;
        }
        g.setFont(new Font("TimesRoman", Font.PLAIN, size));
        g.drawString(t,x,y);
    }

    /**
     * Draws the board
     * @param g 	graphics object
     */
    private void drawBoard( Graphics g){
    	if(this.board == null){
            throw new NullPointerException("No Board was initialised!");
    	} else if(this.board.getGeometry() == Board.SHAPE_RECT){
    		drawRectBoard(g);
    	} else if(this.board.getGeometry() == Board.SHAPE_HEX){
    		drawHexBoard(g);
    	} else if(this.board.getGeometry() == Board.SHAPE_TRIANGLE){
    		drawTriangleBoard(g);
    	}
    }

    private void drawTriangleBoard(Graphics g){
    	//TODO
    }

    /**
     * Paints the necessary components
     * @param g 	graphics object
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //drawBackground(g);
        drawBoard(g);

        g.setColor(Color.RED);

        // BufferedImage image = null;
        // try{
        //     image = ImageIO.read(new File("../../img/bild.png"));
        // }catch(IOException e){
        //     e.printStackTrace();
        // }
        // g.drawImage(image, 500, 0, 80,80, null);
    }


    /**
     * Calculates the euclidean distance of two points
     * @param x1        x coordinate first point
     * @param y1        y coordinate first point
     * @param x2        x coordinate second point
     * @param y2        y coordinate second point
     * @return          euclidean distance
     */
    private double getDistance( int x1, int y1, int x2, int y2){
        return java.lang.Math.sqrt( (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2) );
    }
}

