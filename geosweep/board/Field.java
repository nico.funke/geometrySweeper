/**
* Class for single fields on the board
* @author Nico Funke
*/

package geosweep.board;

public class Field{

  private boolean bomb;
  private int status;     // if status > 0 it is the number to show

  public static final int HIDDEN    = -1;
  public static final int MARKED    = -2;
  public static final int QUESTION  = -3;
  public static final int EXPLODED  = -4;
  public static final int OUTSIDE   = -5;

  /**
   * Empty Constructor
   * Sets status to hidden and bomb to false
   **/
  public Field(){
    status  = HIDDEN;
    bomb    = false;
  }

  /**
   * hides the field
   */
  public void hide(){
      if( !isOutside() ){
        this.status = HIDDEN;
      }
  }

  /**
   * Returns if the field is an outside field
   * @return    boolean if it is outside
   */
  public boolean isOutside(){
    return this.status == OUTSIDE;
  }

  /**
   * Returns if the field contains a bomb
   * @return    boolean if this Field is a bomb
   **/
  public boolean isBomb(){
    return this.bomb;
  }

  /**
   * switches the status, from hidden to marked,
   * from marked to questino or from question to hidden
   * otherwise nothing happens
   **/
   public void switchStatus(){
     switch(status){
       case HIDDEN:   status = MARKED;
                      break;
       case MARKED:   status = QUESTION;
                      break;
       case QUESTION: status = HIDDEN;
                      break;
       default: break;
     }
   }

   /**
    * Returns the status of the field
    * @return    status of the field
    */
   public int getStatus(){
     return this.status;
   }

   /**
    * Returns if the field is hidden, marked or question
    * @return   boolean if the field is hidden, marked or question
    */
   public boolean isHidden(){
     return status == HIDDEN || status == MARKED || status == QUESTION;
   }

   /**
    * Changes the status of the Field
    * @param status     new status
    */
   public void setStatus( int status ){
     this.status = status;
   }

   /**
    * Changes if the field contains a bomb
    * @param bomb   boolean if the field contains a bomb
    */
   public void setBomb(boolean bomb){
    this.bomb = bomb;
   }

   /**
    * Returns a string interpretation of the field
    * @return     field as a string
    */
   public String toString(){
    if(this.isBomb()){
      return "B";
    }
    switch(this.status){
      case HIDDEN:    return "#";
      case MARKED:    return "!";
      case QUESTION:  return "?";
      case OUTSIDE:   return " ";
      case EXPLODED:  return "X";
      default:        return ""+this.status;
    }
   }
}
