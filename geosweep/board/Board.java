/**
* class for the board
* @author Nico Funke
*/

/* TODO: 
       - maybe a method with array input?
*/

package geosweep.board;

import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class Board{

  private int       geometry;
  private Field[][] cluster;
  private int       status;
  private int       bombs;
  private int       clicks;
  private Stack<int[][]> lastBoards = new Stack<int[][]>();
  private boolean bombsPlaced = false;

  public static final int SHAPE_RECT      = 4;
  public static final int SHAPE_TRIANGLE  = 3;
  public static final int SHAPE_HEX       = 6;

  public static final int STATUS_WIN      = 1;
  public static final int STATUS_LOSE     = 2;
  public static final int STATUS_GAME     = 3;

  /**
   * Constructor with size and shape
   * @param x     board width
   * @param y     board height
   * @param bombs bombs to place
   * @param shape geometric shape of the Board
   */
  public Board(int x, int y, int geo, int bombs) throws IllegalArgumentException{
    if(bombs > x*y){
      throw new IllegalArgumentException("More bombs than exisiting fields.");
    }
    this.cluster   = emptyField(x,y);
    this.geometry  = geo;
    this.status    = STATUS_GAME;
    this.bombs     = bombs;
    //this.placeBombs();
    this.clicks    = 0;
    lastBoards.push(getView());
  }

/******************** PRIVATE METHODS ************************/
  
  /**
   * Creates an empty 2D Array of Fields
   * @param x    width of the array
   * @param y    height of the array
   * @return    empty 2D array of fields
   */
  private Field[][] emptyField( int x, int y){
    Field[][] out = new Field[x][y];
    for(int i = 0; i < x; i++){
      for(int j = 0; j< y ; j++){
        out[i][j] = new Field();
      }
    }
    return out;
  }

  /**
   * Returns the number of bombs in the neighbours of the field
   * @param x   x position of the field
   * @param y   y position of the field
   * @return    number of bombs
   */
  private int getNeighbouringBombs(int x, int y){
      ArrayList<Integer[]> neighbours = getNeighbours(x,y);
      Iterator<Integer[]> itr = neighbours.iterator();
      int b = 0;
      while (itr.hasNext()) {
        Integer[] tmp = itr.next();
        b += cluster[tmp[0]][tmp[1]].isBomb()?1:0;
      }
      return b;
  }

  /**
   * Reveals all neighbours of a field
   * Calls the fitting method for that shape
   * @param x   x position of the field
   * @param y   y position of the field
   */
  private void revealNeighbours(int x, int y){
    ArrayList<Integer[]> neighbours = getNeighbours(x,y);
    Iterator<Integer[]> itr = neighbours.iterator();
     while (itr.hasNext()) {
       Integer[] tmp = itr.next();
       reveal(tmp[0],tmp[1]);
      }
      return;
  }

  /**
   * Calls the reveal method on all neighbours, in rect shape
   * @param x     x position of the field
   * @param y     y position of the field
   */
  private void revealRectNeighbours(int x,int y){
    if( x != 0){
     reveal(x-1,y);
      if( y != 0){
        reveal(x-1,y-1);
      }
      if( y+1 < this.getHeight()){
        reveal(x-1,y+1);
      }
    }
    if( x+1 < this.getWidth()){
      reveal(x+1,y);
      if( y != 0){
        reveal(x+1,y-1);
      }
      if( y+1 < this.getHeight()){
        reveal(x+1,y+1);
      }
    }
    if( y != 0){
      reveal(x,y-1);
    }
    if( y+1 < this.getHeight()){
      reveal(x,y+1);
    }
  }

  private ArrayList<Integer[]> getTriangleNeighbours(int x,int y){
    // TODO
    return new ArrayList<Integer[]>();
  }

  /**
   * Returns an arraylist of all neighbours, if the shape is rect
   * @param x   x position of the field
   * @param y   y positino of the field
   * @return    arraylist of neighbours
   */
  private ArrayList<Integer[]> getRectNeighbours(int x,int y){
    ArrayList<Integer[]> out = new ArrayList<Integer[]>();
    if( x != 0){
     Integer[] tmp = {x-1,y};
     out.add(tmp);
      if( y != 0){
        Integer[] tmp2 = {x-1,y-1}; 
        out.add(tmp2);
      }
      if( y+1 < this.getHeight()){
        Integer[] tmp2 = {x-1,y+1}; 
        out.add(tmp2);
      }
    }
    if( x+1 < this.getWidth()){
      Integer[] tmp = {x+1,y};
      out.add(tmp);
      if( y != 0){
        Integer[] tmp2 = {x+1,y-1}; 
        out.add(tmp2);
      }
      if( y+1 < this.getHeight()){
        Integer[] tmp2 = {x+1,y+1}; 
        out.add(tmp2);
      }
    }
    if( y != 0){
      Integer[] tmp = {x,y-1}; 
      out.add(tmp);
    }
    if( y+1 < this.getHeight()){
      Integer[] tmp = {x,y+1}; 
      out.add(tmp);
    }

    return out;
  }

  /**
   * Returns the neighbours, if board is hex shaped
   * @param x   x position of the field
   * @param y   y position of the field
   * @return    Arraylist of all neighbours
   * TODO does not work
   */
  private ArrayList<Integer[]> getHexNeighbours(int x,int y){
    ArrayList<Integer[]> out = new ArrayList<Integer[]>();
    
    if( y+1 < this.getHeight()){
      Integer[] tmp1 = {x,y+1}; 
      out.add(tmp1);
      if(y+2 < this.getHeight()){
        Integer[] tmp2 = {x,y+2}; 
        out.add(tmp2);      
      }
    }

    if(y-1 >= 0){
      Integer[] tmp3 = {x, y-1};
      out.add(tmp3);
      if(y-2 >= 0){
        Integer[] tmp4 = {x,y-2};
        out.add(tmp4);       
      }
    }

    if( y%2 != 0 && x-1 >= 0){    // Even rows
      if(y-1 >= 0){
        Integer[] tmp5 = {x-1,y-1}; 
        out.add(tmp5);
      }
      if(y+1 < this.getHeight()){
        Integer[] tmp6 = {x-1,y+1}; 
        out.add(tmp6);
      }
    } else if( y%2 == 0 && x+1 <this.getWidth()){ // odd rows
      if(y-1 >= 0){
        Integer[] tmp5 = {x+1,y-1}; 
        out.add(tmp5);
      }
      if(y+1 < this.getHeight()){
        Integer[] tmp6 = {x+1,y+1}; 
        out.add(tmp6);
      }
    }
    return out;
  }

  /**
   * Returns the neighbours of a field
   * @param x   x position of the field
   * @param y   y position of the field
   * @return    arraylist of the neighbours as array of their coordinates
   */
  private ArrayList<Integer[]> getNeighbours(int x, int y){
    if( this.geometry == SHAPE_RECT){
      return getRectNeighbours(x,y);
    } else if (this.geometry == SHAPE_TRIANGLE){
      return getTriangleNeighbours(x,y);
    } else if (this.geometry == SHAPE_HEX){
      return getHexNeighbours(x,y);
    } else {
      throw new IllegalArgumentException("Wrong or unknown shape!");
    }
  }

  /**
   * evaluates the board status
   */
  private void evaluate(){
    int x = this.getWidth();
    int y = this.getHeight();
    int hidden = 0;
    for( int i = 0; i < x; i++){
      for( int j = 0; j < y; j++){
        if(cluster[i][j].getStatus() == Field.EXPLODED){
          this.status = STATUS_LOSE;
          return;
        }
        hidden += cluster[i][j].isHidden()?1:0;
      }
    }
    if( hidden == bombs){
      this.status = STATUS_WIN;
    } else {
      this.status = STATUS_GAME;
    }
  }

  /**
   * Reveals the field and other fields recursively if necessary
   * @param x   x position of the field
   * @param y   y position of the field
   */
  private void reveal(int x, int y){
      if( cluster[x][y].isOutside() || !cluster[x][y].isHidden()){
        return;
      }
      int b = getNeighbouringBombs(x,y);
      cluster[x][y].setStatus(b);
      if( b == 0){
        revealNeighbours(x,y);
      }
  }

  /**
   * Places the bombs randomly on the board
   */
  private void placeBombs(){
    int maxX    = this.getWidth();
    int maxY    = this.getHeight();
    Random rand = new Random();
    for (int i = 0; i < bombs ;i++){
      int x,y;
      do{
        x = rand.nextInt(maxX);
        y = rand.nextInt(maxY);
      }while( cluster[x][y].isOutside() || cluster[x][y].isBomb() || cluster[x][y].getStatus() == Field.QUESTION);
      cluster[x][y].setBomb(true);
    }
    bombsPlaced = true;
  }

  /**
   * restores the board from a view
   * @param view    view to be restored
   */
  private void restoreBoard(int[][] view){
    int x = this.getWidth();
    int y = this.getHeight();
    for( int i = 0; i < x; i++){
      for( int j = 0; j < y; j++){
        cluster[i][j].setStatus(view[i][j]);
      }
    }
  }

  /**
   * Performs the first click on a given field
   * After this all neighbours will get no bombs and the bombs are placed
   * @param x   x position of the click
   * @param y   y position of the field
   */
  private void firstClick(int x, int y){
    cluster[x][y].setStatus(Field.QUESTION);
    ArrayList<Integer[]> neighbours = getNeighbours(x,y);
    while(bombs + neighbours.size()+1 > getWidth() * getHeight()){
      neighbours.remove(neighbours.size()-1);
    }
    Iterator<Integer[]> itr = neighbours.iterator();
    while (itr.hasNext()) {
      Integer[] tmp = itr.next();
      cluster[tmp[0]][tmp[1]].setStatus(Field.QUESTION);
    }
    placeBombs();
    cluster[x][y].setStatus(Field.HIDDEN);
    neighbours = getNeighbours(x,y);
    itr = neighbours.iterator();
    while (itr.hasNext()) {
      Integer[] tmp = itr.next();
      cluster[tmp[0]][tmp[1]].setStatus(Field.HIDDEN);
    }
  }

/***************** PUBLIC METHODS **************************/

/**
 * Returns a simple String version of the board
 * @return    board as a string
 */
public String toString(){
    int maxX    = this.getWidth();
    int maxY    = this.getHeight();
    String out = "";
    for(int y = 0; y < maxY ; y++){
      if(y % 2 == 0){ // --- Even column
        if( this.geometry == SHAPE_HEX){
          out += " ";
        }
      }
      for(int x = 0; x < maxX ; x++){
        out += " " +cluster[x][y].toString();
      }
      out += "\n";
    }
    return out;
}

/**
 * resets the board, but keeps the bombs
 */
public void reset(){
  for( int i = 0; i < getWidth(); i++){
    for( int j = 0; j < getHeight(); j++){
      cluster[i][j].hide();
    }
  }
  clicks = 0;
  lastBoards = new Stack<int[][]>();
  evaluate();
}

  /**
   * Clicks the field, therefor a bomb explodes or field get revealed
   * @param x   x position of the field
   * @param y   y position of the field
   */
  public void click(int x, int y){
    if(cluster[x][y].isOutside() 
        || cluster[x][y].getStatus() == Field.MARKED
        || !cluster[x][y].isHidden()
        || status != STATUS_GAME){
      return;
    }
    if(!bombsPlaced){ // First Click
      firstClick(x,y);
    }
    lastBoards.push(getView());
    clicks++;
    if (cluster[x][y].isBomb()){
      cluster[x][y].setStatus(Field.EXPLODED);
    } else {
      this.reveal(x,y);
    }
    this.evaluate();
  }

  /**
   * Double clicks the field
   * @param x   x position of the field
   * @param y   y position of the field
   */
  public void doubleClick(int x, int y){
    if(cluster[x][y].isOutside() 
        || cluster[x][y].isHidden()
        || status != STATUS_GAME){
      return;
    }
    int b = getNeighbouringBombs(x,y);
    ArrayList<Integer[]> n = getNeighbours(x,y);
    Iterator<Integer[]> itr = n.iterator();
    int marked = 0;
    while (itr.hasNext()) {
      Integer[] tmp = itr.next();
      Field f = cluster[tmp[0]][tmp[1]];
      if(f.isHidden() && f.getStatus() != Field.HIDDEN){ // MARKED OR QUESTION
        marked++;
      }
    }
    if( b != marked){
      return;
    }
    lastBoards.push(getView());
    itr = n.iterator();
    while (itr.hasNext()) {
      Integer[] tmp = itr.next();
      Field f = cluster[tmp[0]][tmp[1]];
      if(f.getStatus() == Field.HIDDEN){ // MARKED OR QUESTION
        click(tmp[0],tmp[1]);
      }
    }
  }

  /**
   * Marks the field
   * @param x   x position of the field
   * @param y   y position of the field
   */
  public void mark(int x, int y){
    if(cluster[x][y].isOutside()
      || status != STATUS_GAME){
      return;
    }
    cluster[x][y].switchStatus();
    this.evaluate();
  }  

  /**
   * Returns an integer array represantation of the Board
   * @return    integer array containing values as in Field
   */
  public int[][] getView(){
    int x = this.getWidth();
    int y = this.getHeight();
    int[][] out = new int[x][y];
    for( int i = 0; i < x; i++){
      for( int j = 0; j < y; j++){
        out[i][j] = cluster[i][j].getStatus();
      }
    }
    return out;
  }

  /**
   * Returns the width of the board
   * @return  width of the Board
   */
  public int getWidth(){
    return cluster.length;
  }

  /**
   * Returns the height of the board
   * @return  height of the Board
   */
  public int getHeight(){
    return cluster[0].length;
  }

  /**
   * Returns the shape of the fields
   * @return    shape of the fields
   */
  public int getGeometry(){
    return this.geometry;
  }

  /**
   * Returns the number of clicks
   * @return number of clicks
   */
  public int getClicks(){
    return this.clicks;
  }

  /**
   * Returns the status
   * @return status
   */
  public int getStatus(){
    return this.status;
  }

  /**
   * undoes the last step
   */
  public void undo(){
    if(lastBoards.isEmpty()){
      return;
    }
    restoreBoard(lastBoards.pop());
    if(clicks != 0){
      clicks--;
    }
    evaluate();
  }
}
