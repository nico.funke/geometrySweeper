# GeometrySweeper
**Autor:** Nico Funke
2018

## Beschreibung
Eine Minesweeper Variante mit verschiedenen Formen. Bisher leider nur Hexagons und Quadrate, eine Version mit Dreiecken folgt aber. Die Idee des Projekts ist ein Spiel für die [CodeSweeper Competition](https://www.get-in-it.de/competition/codesweeper?utm_source=facebook&utm_medium=cpc&utm_campaign=competition&utm_content=codesweeper).  
Eine Anleitung zu Minesweeper findet sich [hier](https://www.bernhard-gaul.de/spiele/minesweeper/minesweeper-spielregel.html).
Das Spiel lässt sich komplett einfach über die graphische Ausgabe und den Launcher konfigurieren und spielen.

## Installation
Über das buildfile lässt sich das Programm einfach mit dem Aufruf "ant" ind den Ordner bin/ kompilieren.  
Mit dem Aufruf "ant jar" wird direkt ein ausführbares jar Archiv generiert.  
Über den Aufruf "ant docs" lässt sich eine Dokumentation des Projekts in den Ordner doc/ erzeugen.

## Bemerkung
Ich bin leider kein Designer, was man vielleicht am Menu merkt...

